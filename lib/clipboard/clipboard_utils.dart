import 'package:flutter/services.dart';

class ClipboardUtils {
  static const String TAG = "clipboard";

  static Future<void> copyToClipboard({required String text}) async {
    return await Clipboard.setData(ClipboardData(text: text));
  }
}
