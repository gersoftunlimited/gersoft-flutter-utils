// 🐦 Flutter imports:
import 'package:flutter/material.dart';

// Remove all the focus on the page
class FocusUtils {
  static void unfocusAll({required BuildContext context}) => FocusScope.of(context).requestFocus(FocusNode());
}
