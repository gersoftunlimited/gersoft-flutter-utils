import 'package:flutter_email_sender/flutter_email_sender.dart';

class EmailUtils {
  static Future<bool> send(List<String> recipients) async {
    return await sendWithAllFieldsEmpty(recipients);
  }

  static Future<bool> sendWithAllFieldsEmpty(List<String> recipients,
      {Function()? onSendEmailError}) async {
    final Email email = Email(
      recipients: recipients,
      isHTML: false,
    );

    return await FlutterEmailSender.send(email)
        .then((value) => true)
        .catchError((error) => false);
  }
}
