// ignore_for_file: constant_identifier_names

import 'dart:io';

import 'package:map_launcher/map_launcher.dart';
import 'package:utils/url_launcher/url_launcher.dart';

class MapUtils {
  static Future<void> goCoordinates(
      {required double latitude,
      required double longitude,
      String locationName = "",
      required Function() onGoCoordinatesError}) async {
    Coords coords = Coords(latitude, longitude);
    if (Platform.isAndroid) {
      bool? mapsAvailable = await MapLauncher.isMapAvailable(MapType.google);

      if (mapsAvailable!) {
        await MapLauncher.showMarker(
            mapType: MapType.google, coords: coords, title: locationName);
      } else {
        onGoCoordinatesError();
      }
    } else if (Platform.isIOS) {
      bool? mapsAvailable = await MapLauncher.isMapAvailable(MapType.apple);

      if (mapsAvailable!) {
        await MapLauncher.showMarker(
            mapType: MapType.apple, coords: coords, title: locationName);
      } else {
        onGoCoordinatesError();
      }
    } else {
      UrlLauncher.launchURLoutsideApp(
          "https://www.google.com/maps/search/?api=1&query=$latitude,$longitude");
    }
  }
}
