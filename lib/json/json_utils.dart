// 🎯 Dart imports:
import 'dart:convert';

class JsonUtils {
  static decode(response) {
    return json.decode(utf8.decode(response.bodyBytes));
  }

  static encode(Map<dynamic, dynamic> json) {
    return jsonEncode(json);
  }
}
