import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';

class PhoneUtils {
  static Future<bool> call(String phoneNumber, {bool prefix = true}) async {
    return await FlutterPhoneDirectCaller.callNumber(phoneNumber)
        .then((value) => true)
        .catchError((error) => false);
  }
}
