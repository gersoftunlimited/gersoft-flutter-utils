import 'dart:developer';

import 'package:url_launcher/url_launcher.dart';


class UrlLauncher {
  static Future<bool> launchURLoutsideApp(String urlString,
      {addHttp = true}) async {
    if (urlString.isEmpty) {
      return false;
    }

    urlString = urlString.replaceAll("\n", " ").trim();
    if (!urlString.startsWith("http") && addHttp)
      urlString = "https://$urlString";

    final uri = Uri.encodeFull(urlString);
    log("Url launcher $urlString");

    return launch(uri, headers: {
      'User-Agent':
          "Mozilla/5.0 (iPhone; CPU iPhone OS 9_3 like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Version/9.0 Mobile/13E233 Safari/601.1"
    }).onError((error, stackTrace) => false);
  }
}
