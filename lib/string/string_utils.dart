// 📦 Package imports:
import 'package:diacritic/diacritic.dart';
import 'package:html/parser.dart';
import 'package:html_character_entities/html_character_entities.dart';

enum StringType {
  normal,
  lowercase,
  uppercase,
  capitalizeFirst,
  capitalize,
  removeDiacritics
}

class StringUtils {
  static capitalizeFirstLetter(text) {
    if (text.length < 1) return text;

    return "${text[0].toUpperCase()}${text.substring(1).toLowerCase()}";
  }

  static capitalize(String text) {
    if(!text.contains(" ")) {
      return capitalizeFirstLetter(text);
    }

    var fullTextSplitted = text.split(" ");
    String textToReturn = "";

    for (var i = 0; i < fullTextSplitted.length; i++) {
      String valueSplitted = fullTextSplitted[i];
      textToReturn = "${textToReturn + capitalizeFirstLetter(valueSplitted)} ";
    }

    if(textToReturn.endsWith(" ")) {
      textToReturn = textToReturn.substring(0, textToReturn.length - 1);
    }

    return textToReturn;
  }

  static String format(
      {required String text, StringType type = StringType.normal}) {
    switch (type) {
      case StringType.lowercase:
        return text.toLowerCase();
      case StringType.uppercase:
        return text.toUpperCase();
      case StringType.capitalizeFirst:
        return capitalizeFirstLetter(text);
      case StringType.capitalize:
        return capitalize(text);
      case StringType.removeDiacritics:
        return removeDiacritics(text);
      default:
        return text;
    }
  }

//here goes the function
  static String parseHtmlString(String htmlString) {
    final document = parse(htmlString);
    final String parsedString =
        parse(document.body!.text).documentElement!.text;

    return parsedString;
  }

  String decodeHtmlString(String text) {
    return HtmlCharacterEntities.decode(text);
  }

  static bool checkEmptyOrNull(String? text) {
    return text == null ? false : text.isEmpty;
  }
}
