class LanguageUtils {
  static String _language = "ES";

  static void setLanguage({required language}) {
    if (_language.isNotEmpty) {
      _language = language.toUpperCase();
    } else {
      _language = "ES";
    }
  }

  static String getLanguage() => _language;

  static String getLocale() => _language == "VA" ? "CA" : _language;
}
