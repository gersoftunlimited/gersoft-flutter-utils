// 🐦 Flutter imports:
import 'dart:io';

import 'package:file_utils/export.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

// 📦 Package imports:
import 'package:share/share.dart';

class ShareUtils {
  static Future<void> shareLink({
    String text = "",
    String? subject,
    Rect? sharePositionOrigin,
  }) {
    return Share.share(text,
        subject: subject, sharePositionOrigin: sharePositionOrigin);
  }

  static Future<void> shareFiles({
    List<String> filePaths = const [],
    List<String>? mimeTypes,
    String? subject,
    String? text,
    Rect? sharePositionOrigin,
  }) {
    return Share.shareFiles(filePaths,
        mimeTypes: mimeTypes,
        subject: subject,
        text: text,
        sharePositionOrigin: sharePositionOrigin);
  }

  static Future<void> shareItemFromNetwork({
    String url = "",
    String fileName = "",
    String fileExtension = "",
    String? subject,
    String? text,
    Rect? sharePositionOrigin,
  }) async {
    File? file = await FileUtils.downloadFile(
        url: url, fileName: fileName, fileExtension: fileExtension);

    if (file == null) {
      Fluttertoast.showToast(
          msg: "Error",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 16.0);
      return;
    }

    return Share.shareFiles([file.path],
        subject: subject, text: text, sharePositionOrigin: sharePositionOrigin);
  }
}
