// 📦 Package imports:
import 'package:intl/intl.dart';

class DateTimeUtils {
  static String parseDateTimeToDay(DateTime? dateTime, String locale,{showHours = false}) {
    if (dateTime == null) return "";

    if (showHours) {
      return DateFormat('EEEE, d MMM yyyy - HH:mm', locale)
          .format(dateTime);
    }
    return DateFormat('EEEE, d MMM, yyyy', locale)
        .format(dateTime);
  }

  static String parseDateToDDMMYYYY(String date, String locale, {bool showHours = false}) {
    if(date == null || date.isEmpty) {
      return "";
    }
    try {
      DateTime dateTime = DateTime.parse(date);

      if (showHours) {
        return DateFormat('dd/MM/yyyy HH:mm', locale)
            .format(dateTime);
      } else {
        return DateFormat('dd/MM/yyyy', locale)
            .format(dateTime);
      }
    } catch (error) {
      return "";
    }
  }

  static bool dateIsBetween(
      {DateTime? startDate, DateTime? endDate, required DateTime dateTime}) {
    if (endDate == null && startDate == null) {
      return true;
    }

    endDate = endDate ?? getCurrentDateWithoutHours(dateTime: DateTime.now());
    endDate = endDate
        .add(const Duration(days: 1))
        .subtract(const Duration(seconds: 1));

    if (startDate == null) {
      return endDate.isAfter(dateTime) || dateTime == endDate;
    }

    return (startDate.isBefore(dateTime) || dateTime == startDate) &&
        (endDate.isAfter(dateTime) || dateTime == endDate);
  }

  static parseDateTimeToHours({DateTime? dateTime}) {
    if (dateTime == null) return "";

    return DateFormat('HH:mm').format(dateTime);
  }

  static DateTime getCurrentDateWithoutHours({required DateTime dateTime}) {
    return DateTime(dateTime.year, dateTime.month, dateTime.day, 0, 0, 0, 0, 0);
  }

  static DateTime getCurrentDateWithHours({required DateTime dateTime}) {
    return DateTime(dateTime.year, dateTime.month, dateTime.day, dateTime.hour,
        dateTime.minute, 0, 0, 0);
  }

  static getDatesBetween(
      {required DateTime startDate, required DateTime endDate}) {
    List<DateTime> days = [];

    if (isSameDay(startDate: startDate, endDate: endDate)) return [startDate];

    for (int i = 0; i <= endDate.difference(startDate).inDays; i++) {
      days.add(startDate.add(Duration(days: i)));
    }
    return days;
  }

  static isSameDay({required DateTime startDate, required DateTime endDate}) {
    return startDate.day == endDate.day;
  }

  static bool dateSelectedIsDifferentMonth(
      {required DateTime daySelected, required DateTime lastDaySelected}) {
    return daySelected.month != lastDaySelected.month ||
        daySelected.year != lastDaySelected.year;
  }

  static DateTime getFirstDayMonthFromSelectedDay(
      {required DateTime selectedDay}) {
    return DateTime(selectedDay.year, selectedDay.month, 1, 0, 0, 0, 0);
  }

  static isBetweenDates(
      {required DateTime startDay,
      required DateTime endDay,
      required DateTime currentDay}) {
    return dateIsBefore(startDay: startDay, currentDay: currentDay) &&
        dateIsAfter(endDay: endDay, currentDay: currentDay);
  }

  static dateIsBefore(
      {required DateTime startDay, required DateTime currentDay}) {
    return (startDay.isBefore(currentDay) || startDay == currentDay);
  }

  static dateIsAfter({required DateTime endDay, required DateTime currentDay}) {
    return (endDay.isAfter(currentDay) || endDay == currentDay);
  }

  static String parseDateTimeToRequest(DateTime? dateTime) {
    if (dateTime == null) return "";

    String dateTimeToRequest =
        DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").format(dateTime);
    Duration offset = dateTime.timeZoneOffset;
    int hours =
        offset.inHours > 0 ? offset.inHours : 1; // For fixing divide by 0

    if (!offset.isNegative) {
      dateTimeToRequest =
          "$dateTimeToRequest+${offset.inHours.toString().padLeft(2, '0')}:${(offset.inMinutes % (hours * 60)).toString().padLeft(2, '0')}";
    } else {
      dateTimeToRequest =
          "$dateTimeToRequest-${(-offset.inHours).toString().padLeft(2, '0')}:${(offset.inMinutes % (hours * 60)).toString().padLeft(2, '0')}";
    }

    return dateTimeToRequest;
  }
}
